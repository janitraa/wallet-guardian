package com.Project.repository;

import com.Project.model.KategoriPengeluaran;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface KategoriPengeluaranRepository extends CrudRepository<KategoriPengeluaran, Long> {

    ArrayList<KategoriPengeluaran> findByPenggunaOrderByIdAsc(String pengguna);

    void deleteById(long id);
}
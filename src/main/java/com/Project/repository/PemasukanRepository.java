package com.Project.repository;

import com.Project.model.Pemasukan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;

@Repository
public interface PemasukanRepository extends CrudRepository<Pemasukan, Long> {
    ArrayList<Pemasukan> findByPenggunaOrderByIdAsc(String pengguna);
    ArrayList<Pemasukan> findByPenggunaAndKategoriGreaterThanEqualOrderByIdAsc(String pengguna, String kategori);
    ArrayList<Pemasukan> findByPenggunaAndTanggalCatatanGreaterThanEqualOrderByIdAsc(String pengguna, Date tanggalCatatan);

    void deleteById(long id);

}


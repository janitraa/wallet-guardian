package com.Project.repository;

import com.Project.model.KategoriPemasukan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface KategoriPemasukanRepository extends CrudRepository<KategoriPemasukan, Long> {

    ArrayList<KategoriPemasukan> findByPenggunaOrderByIdAsc(String pengguna);

    void deleteById(long id);
}
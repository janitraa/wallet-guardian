package com.Project.repository;

import com.Project.model.Pengeluaran;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;

@Repository
public interface PengeluaranRepository extends CrudRepository<Pengeluaran, Long> {
    ArrayList<Pengeluaran> findByPenggunaOrderByIdAsc(String pengguna);
    ArrayList<Pengeluaran> findByPenggunaAndKategoriGreaterThanEqualOrderByIdAsc(String pengguna, String kategori);
    ArrayList<Pengeluaran> findByPenggunaAndTanggalCatatanGreaterThanEqualOrderByIdAsc(String pengguna, Date tanggalCatatan);

    void deleteById(long id);
}

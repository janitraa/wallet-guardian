package com.Project.Controller;

import com.Project.model.Pemasukan;
import com.Project.model.Pengeluaran;
import com.Project.model.Transaksi;
import com.Project.repository.PemasukanRepository;
import com.Project.repository.PengeluaranRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin
@RestController
public class TransaksiController {

    @Autowired
    PengeluaranRepository pengeluaranRepository;

    @Autowired
    PemasukanRepository pemasukanRepository;

    @RequestMapping(value = "addtransaksi", method = RequestMethod.POST)
    @ResponseBody
    public List<Transaksi> postPengeluaran(@RequestParam String tipe,
                                  @RequestParam String judul,
                                  @RequestParam String kategori,
                                  @RequestParam int jumlah,
                                  @RequestParam String pengguna) {

        LocalDate tanggalSubmit = LocalDate.now();
        Date konversiTanggalSubmit = java.sql.Date.valueOf(tanggalSubmit);
        if (tipe.equalsIgnoreCase("Pengeluaran")) {
            Pengeluaran transaksi = new Pengeluaran(konversiTanggalSubmit ,judul,kategori,jumlah, pengguna);
            pengeluaranRepository.save(transaksi);
        } else {
            Pemasukan transaksi = new Pemasukan(konversiTanggalSubmit, judul,kategori,jumlah, pengguna);
            pemasukanRepository.save(transaksi);
        }
        return getDaftarTransaksi(pengguna);
    }

    public List<Transaksi> getDaftarTransaksi(String pengguna) {
        List<Transaksi> daftarTransaksi = new ArrayList<Transaksi>();
        daftarTransaksi.addAll(pengeluaranRepository.findByPenggunaOrderByIdAsc(pengguna));
        daftarTransaksi.addAll(pemasukanRepository.findByPenggunaOrderByIdAsc(pengguna));
        return daftarTransaksi;
    }

    @RequestMapping(value = "get-all-pengeluaran", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<Pengeluaran> getAllPengeluaran() {

        return pengeluaranRepository.findAll();
    }

    @RequestMapping(value = "get-all-pemasukan", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<Pemasukan> getAllPemasukan() {

        return pemasukanRepository.findAll();
    }

    @RequestMapping(value = "delete-pemasukan/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ArrayList<Pemasukan> deletePemasukan(@PathVariable long id, @RequestParam String pengguna) {
        pemasukanRepository.deleteById(id);
        return pemasukanRepository.findByPenggunaOrderByIdAsc(pengguna);
    }

    @RequestMapping(value = "delete-pengeluaran/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ArrayList<Pengeluaran> deletePengeluaran(@PathVariable long id, @RequestParam String pengguna) {
        pengeluaranRepository.deleteById(id);
        return pengeluaranRepository.findByPenggunaOrderByIdAsc(pengguna);
    }

    @RequestMapping(value = "get-pengeluaran/{pengguna}", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<Pengeluaran> getPengeluaran(@PathVariable String pengguna) {

        return pengeluaranRepository.findByPenggunaOrderByIdAsc(pengguna);
    }

    @RequestMapping(value = "get-pemasukan/{pengguna}", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<Pemasukan> getPemasukan(@PathVariable String pengguna) {
        return pemasukanRepository.findByPenggunaOrderByIdAsc(pengguna);
    }
}

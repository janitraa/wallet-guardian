package com.Project.Controller;

import com.Project.model.Kategori;
import com.Project.model.KategoriPemasukan;
import com.Project.model.KategoriPengeluaran;
import com.Project.repository.KategoriPemasukanRepository;
import com.Project.repository.KategoriPengeluaranRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
public class KategoriController {

    @Autowired
    KategoriPengeluaranRepository kategoriPengeluaranRepository;

    @Autowired
    KategoriPemasukanRepository kategoriPemasukanRepository;


    @RequestMapping(value = "addKategori", method = RequestMethod.POST)
    @ResponseBody
    public List<Kategori> postKategori(@RequestParam String tipe,
                                           @RequestParam String nama,
                                           @RequestParam String pengguna) {

        if (tipe.equalsIgnoreCase("Pengeluaran")) {
            KategoriPengeluaran kategori = new KategoriPengeluaran(tipe, nama, pengguna);
            kategoriPengeluaranRepository.save(kategori);
        } else {
            KategoriPemasukan kategori = new KategoriPemasukan(tipe, nama, pengguna);
            kategoriPemasukanRepository.save(kategori);
        }
        return getDaftarKategori(pengguna);
    }

    public List<Kategori> getDaftarKategori(String pengguna) {
        List<Kategori> daftarKategori = new ArrayList<Kategori>();
        daftarKategori.addAll(kategoriPengeluaranRepository.findByPenggunaOrderByIdAsc(pengguna));
        daftarKategori.addAll(kategoriPemasukanRepository.findByPenggunaOrderByIdAsc(pengguna));
        return daftarKategori;
    }

    @RequestMapping(value = "get-kategori-pengeluaran", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<KategoriPengeluaran> getKategoriPengeluaran() {

        return kategoriPengeluaranRepository.findAll();
    }

    @RequestMapping(value = "get-kategori-pemasukan", method = RequestMethod.GET)
    @ResponseBody
    public Iterable<KategoriPemasukan> getKategoriPemasukan() {

        return kategoriPemasukanRepository.findAll();
    }

    @RequestMapping(value = "delete-kategori-pemasukan/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ArrayList<KategoriPemasukan> deleteKategoriPemasukan(@PathVariable long id, @RequestParam String pengguna) {
        kategoriPemasukanRepository.deleteById(id);
        return kategoriPemasukanRepository.findByPenggunaOrderByIdAsc(pengguna);
    }

    @RequestMapping(value = "delete-kategori-pengeluaran/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ArrayList<KategoriPengeluaran> deletePengeluaran(@PathVariable long id, @RequestParam String pengguna) {
        kategoriPengeluaranRepository.deleteById(id);
        return kategoriPengeluaranRepository.findByPenggunaOrderByIdAsc(pengguna);
    }

    @RequestMapping(value = "get-pengeluaran-kategori/{pengguna}", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<KategoriPengeluaran> getPengeluaranKategori(@PathVariable String pengguna) {

        return kategoriPengeluaranRepository.findByPenggunaOrderByIdAsc(pengguna);
    }

    @RequestMapping(value = "get-pemasukan-kategori/{pengguna}", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<KategoriPemasukan> getPemasukanKategori(@PathVariable String pengguna) {
        return kategoriPemasukanRepository.findByPenggunaOrderByIdAsc(pengguna);
    }

}

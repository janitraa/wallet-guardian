package com.Project.Controller;

import com.Project.model.Kategori;
import com.Project.model.Pemasukan;
import com.Project.model.Pengeluaran;
import com.Project.model.Transaksi;
import com.Project.repository.PemasukanRepository;
import com.Project.repository.PengeluaranRepository;
import com.Project.repository.KategoriPemasukanRepository;
import com.Project.repository.KategoriPengeluaranRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin
@RestController
public class RekapTransaksiController {

    @Autowired
    PemasukanRepository pemasukanRepository;

    @Autowired
    PengeluaranRepository pengeluaranRepository;
    
    @Autowired
    KategoriPemasukanRepository kategoriPemasukanRepository;
    
    @Autowired
    KategoriPengeluaranRepository kategoriPengeluaranRepository;

    @RequestMapping(value = "get-durasi/{pengguna}", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<? extends Transaksi> getDurasi(@PathVariable String pengguna,
                                                    @RequestParam String durasi,
                                                    @RequestParam String jenis) {
        LocalDate dateNow = LocalDate.now();
        if (durasi.equalsIgnoreCase("harian")) {
            Date konversiDateNow = java.sql.Date.valueOf(dateNow);
            if (jenis.equalsIgnoreCase("pemasukan"))
                return pemasukanRepository.findByPenggunaAndTanggalCatatanGreaterThanEqualOrderByIdAsc(pengguna, konversiDateNow);
            else if (jenis.equalsIgnoreCase("pengeluaran"))
                return pengeluaranRepository.findByPenggunaAndTanggalCatatanGreaterThanEqualOrderByIdAsc(pengguna, konversiDateNow);
        }
        else if (durasi.equalsIgnoreCase("mingguan")) {
            LocalDate startWeek = dateNow.with(TemporalAdjusters.previous(DayOfWeek.MONDAY));
            Date konversiStartWeek = java.sql.Date.valueOf(startWeek);
            if (jenis.equalsIgnoreCase("pemasukan"))
                return pemasukanRepository.findByPenggunaAndTanggalCatatanGreaterThanEqualOrderByIdAsc(pengguna, konversiStartWeek);
            else if (jenis.equalsIgnoreCase("pengeluaran"))
                return pengeluaranRepository.findByPenggunaAndTanggalCatatanGreaterThanEqualOrderByIdAsc(pengguna, konversiStartWeek);
        }
        else if (durasi.equalsIgnoreCase("bulanan")) {
            LocalDate startMonth = dateNow.withDayOfMonth(1);
            Date konversiStartMonth = java.sql.Date.valueOf(startMonth);
            if (jenis.equalsIgnoreCase("pemasukan"))
                return pemasukanRepository.findByPenggunaAndTanggalCatatanGreaterThanEqualOrderByIdAsc(pengguna, konversiStartMonth);
            else if (jenis.equalsIgnoreCase("pengeluaran"))
                return pengeluaranRepository.findByPenggunaAndTanggalCatatanGreaterThanEqualOrderByIdAsc(pengguna, konversiStartMonth);
        }

        return null;
    }

    @RequestMapping(value = "get-rekap/{pengguna}", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<? extends Transaksi> getRekap(@PathVariable String pengguna,
                                                   @RequestParam String jenis,
                                                   @RequestParam String kategori) {
        if (jenis.equalsIgnoreCase("pemasukan")) {
            return pemasukanRepository.findByPenggunaAndKategoriGreaterThanEqualOrderByIdAsc(pengguna, kategori);
        } else if (jenis.equalsIgnoreCase("pengeluaran")) {
            return pengeluaranRepository.findByPenggunaAndKategoriGreaterThanEqualOrderByIdAsc(pengguna, kategori);
        }
        return null;
    }
    
    @RequestMapping(value = "get-kategori/{pengguna}", method = RequestMethod.GET)
    @ResponseBody
    public ArrayList<? extends Kategori> getDaftarKategori(@PathVariable String pengguna,
                                                           @RequestParam String jenis) {
        if (jenis.equalsIgnoreCase("pemasukan")) {
            return kategoriPemasukanRepository.findByPenggunaOrderByIdAsc(pengguna);
        } else if (jenis.equalsIgnoreCase("pengeluaran")) {
            return kategoriPengeluaranRepository.findByPenggunaOrderByIdAsc(pengguna);
        }
        return null;
    }
}

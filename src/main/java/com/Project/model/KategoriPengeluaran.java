package com.Project.model;

import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;

@Entity
public class KategoriPengeluaran extends Kategori{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false, unique = false)
    private String tipe;

    @Column(nullable = false, unique = false)
    private String nama;

    @Column(nullable = false, unique = false)
    @Value("${pengguna:}")
    private String pengguna;

    public KategoriPengeluaran(String tipe, String nama, String pengguna) {
        this.tipe = tipe;
        this.nama = nama;
        this.pengguna = pengguna;
    }

    public KategoriPengeluaran() {}

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPengguna() {
        return pengguna;
    }

    public void setPengguna(String pengguna) {
        this.pengguna = pengguna;
    }
}

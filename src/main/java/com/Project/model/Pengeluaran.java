package com.Project.model;

import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Pengeluaran extends Transaksi {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Temporal(TemporalType.DATE)
    private Date tanggalCatatan;

    @Column(nullable = false, unique = false)
    private String Judul;

    @Column(nullable = false, unique = false)
    private String kategori;

    @Column(nullable = false, unique = false)
    private int jumlah;

    @Column(nullable = false, unique = false)
    @Value("${pengguna:}")
    private String pengguna;

    public Pengeluaran(Date tanggalCatatan, String judul, String kategori, int jumlah, String pengguna) {
        Judul = judul;
        this.kategori = kategori;
        this.jumlah = jumlah;
        this.pengguna = pengguna;
        this.tanggalCatatan = tanggalCatatan;
    }

    public Pengeluaran() {}

    public String getJudul() {
        return Judul;
    }

    public void setJudul(String judul) {
        Judul = judul;
    }

    public String getKategori() {
        return kategori;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public String getPengguna() {
        return pengguna;
    }

    public void setPengguna(String pengguna) {
        this.pengguna = pengguna;
    }

    public Date getTanggalCatatan() {
        return tanggalCatatan;
    }

    public void setTanggalCatatan(Date tanggalCatatan) {
        this.tanggalCatatan = tanggalCatatan;
    }
}

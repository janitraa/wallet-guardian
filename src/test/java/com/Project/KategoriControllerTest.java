package com.Project;

import com.Project.Controller.KategoriController;
import com.Project.model.KategoriPemasukan;
import com.Project.model.KategoriPengeluaran;
import com.Project.repository.KategoriPemasukanRepository;
import com.Project.repository.KategoriPengeluaranRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = KategoriController.class, properties = "spring.profiles.active=test")
public class KategoriControllerTest {

    @MockBean
    private KategoriPengeluaranRepository kategoriPengeluaranRepository;

    @MockBean
    private KategoriPemasukanRepository kategoriPemasukanRepository;

    @Autowired
    private MockMvc kategoriController;

    @Test
    public void testGetPengeluaranKategori() throws Exception {
        kategoriController.perform(MockMvcRequestBuilders.get("/get-pengeluaran-kategori/100117"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetPemasukanKategori() throws Exception {
        kategoriController.perform(MockMvcRequestBuilders.get("/get-pemasukan-kategori/100117"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddKategori() throws Exception {
        String tipe = "Pengeluaran";
        String nama = "Transportasi";
        String pengguna = "100117";
        String urlTemplate = "/addKategori?tipe="+ tipe
                + "&nama=" + nama
                + "&pengguna=" + pengguna;
        kategoriController.perform(MockMvcRequestBuilders.post( urlTemplate))
                .andExpect(status().isOk());

        tipe = "Pemasukan";
        nama = "Jual Bolpoin";
        pengguna = "100117";
        urlTemplate = "/addKategori?tipe="+ tipe
                + "&nama=" + nama
                + "&pengguna=" + pengguna;
        kategoriController.perform(MockMvcRequestBuilders.post( urlTemplate))
                .andExpect(status().isOk());
    }


    @Test
    public void testGetKategoriPengeluaran() throws Exception {
        kategoriController.perform(MockMvcRequestBuilders.get("/get-kategori-pengeluaran"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetKategoriPemasukan() throws Exception {
        kategoriController.perform(MockMvcRequestBuilders.get("/get-kategori-pemasukan"))
                .andExpect(status().isOk());
    }

    @Test
    public void testModelKategoriPemasukan() throws Exception {
        KategoriPemasukan dummy = new KategoriPemasukan(
                "Tipe",
                "Nama",
                "Pengguna"
        );
        dummy.setId(2);
        dummy.setTipe("Pemasukan");
        dummy.setNama("Jual Bolpoin");
        dummy.setPengguna("100117");
        Assert.assertEquals(2,dummy.getId());
        Assert.assertEquals("Pemasukan", dummy.getTipe());
        Assert.assertEquals("Jual Bolpoin", dummy.getNama());
        Assert.assertEquals("100117",dummy.getPengguna());

        kategoriController.perform(
                MockMvcRequestBuilders.post("/delete-kategori-pemasukan/2?pengguna=100117"))
                .andExpect(status().isOk());
    }

    @Test
    public void testModelKategoriPengeluaran() throws Exception {
        KategoriPengeluaran dummy = new KategoriPengeluaran(
                "Tipe",
                "Nama",
                "Pengguna"
        );
        dummy.setId(2);
        dummy.setTipe("Pengeluaran");
        dummy.setNama("Transportasi");
        dummy.setPengguna("100117");
        Assert.assertEquals(2,dummy.getId());
        Assert.assertEquals("Pengeluaran", dummy.getTipe());
        Assert.assertEquals("Transportasi", dummy.getNama());
        Assert.assertEquals("100117",dummy.getPengguna());

        kategoriController.perform(
                MockMvcRequestBuilders.post("/delete-kategori-pengeluaran/2?pengguna=100117"))
                .andExpect(status().isOk());
    }
}

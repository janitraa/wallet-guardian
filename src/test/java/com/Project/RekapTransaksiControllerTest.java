package com.Project;

import com.Project.Controller.RekapTransaksiController;
import com.Project.Controller.TransaksiController;
import com.Project.model.Pemasukan;
import com.Project.model.Pengeluaran;
import com.Project.repository.KategoriPemasukanRepository;
import com.Project.repository.KategoriPengeluaranRepository;
import com.Project.repository.PemasukanRepository;
import com.Project.repository.PengeluaranRepository;
import org.assertj.core.util.Arrays;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = RekapTransaksiController.class, properties = "spring.profiles.active=test")
public class RekapTransaksiControllerTest {

    @MockBean
    private PengeluaranRepository pengeluaranRepository;

    @MockBean
    private PemasukanRepository pemasukanRepository;

    @MockBean
    private KategoriPengeluaranRepository kategoriPengeluaranRepository;

    @MockBean
    private KategoriPemasukanRepository kategoriPemasukanRepository;

    @Autowired
    private MockMvc rekapTransaksiController;

    @Test
    public void testGetKategoriPemasukanUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-kategori/100117?jenis=pemasukan"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetKategoriPengeluaranUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-kategori/100117?jenis=pengeluaran"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetKategoriOtherUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-kategori/100117?jenis=other"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetRekapPemasukanUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-rekap/100117?" +
                                                                "jenis=pemasukan&kategori=transportasi"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetRekapPengeluaranUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-rekap/100117?" +
                                                                "jenis=pengeluaran&kategori=transportasi"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetRekapOtherUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-rekap/100117?" +
                                                                "jenis=other&kategori=transportasi"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetDurasiPemasukanHarianUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-durasi/100117?durasi=harian" +
                                                                "&jenis=pemasukan"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetDurasiPengeluaranHarianUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-durasi/100117?durasi=harian" +
                                                                "&jenis=pengeluaran"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetDurasiOtherHarianUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-durasi/100117?durasi=harian" +
                                                                "&jenis=other"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetDurasiPemasukanMingguanUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-durasi/100117?durasi=mingguan" +
                                                                "&jenis=pemasukan"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetDurasiPengeluaranMingguanUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-durasi/100117?durasi=mingguan" +
                                                                "&jenis=pengeluaran"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetDurasiOtherMingguanUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-durasi/100117?durasi=mingguan" +
                                                                "&jenis=other"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetDurasiPemasukanBulananUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-durasi/100117?durasi=bulanan" +
                                                                "&jenis=pemasukan"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetDurasiPengeluaranBulananUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-durasi/100117?durasi=bulanan" +
                                                                "&jenis=pengeluaran"))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetDurasiOtherBulananUser() throws Exception {
        rekapTransaksiController.perform(MockMvcRequestBuilders.get("/get-durasi/100117?durasi=bulanan" +
                                                                "&jenis=other"))
            .andExpect(status().isOk());
    }
}

package com.Project;

import com.Project.Controller.TransaksiController;
import com.Project.model.Pemasukan;
import com.Project.model.Pengeluaran;
import com.Project.repository.PemasukanRepository;
import com.Project.repository.PengeluaranRepository;
import org.assertj.core.util.Arrays;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TransaksiController.class, properties = "spring.profiles.active=test")
public class TransaksiControllerTest {

    @MockBean
    private PengeluaranRepository pengeluaranRepository;

    @MockBean
    private PemasukanRepository pemasukanRepository;

    @Autowired
    private MockMvc transaksiController;

    @Test
    public void testGetPengeluaranUser() throws Exception {
        transaksiController.perform(MockMvcRequestBuilders.get("/get-pengeluaran/100117"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetPemasukanUser() throws Exception {
        transaksiController.perform(MockMvcRequestBuilders.get("/get-pemasukan/100117"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddTransaksi() throws Exception {
        String tipe = "Pengeluaran";
        String judul = "Makan Siang";
        String kategori = "Makanan";
        int jumlah = 20000;
        String pengguna = "100117";
        String urlTemplate = "/addtransaksi?tipe="+ tipe
                            + "&judul=" + judul
                            + "&kategori=" + kategori
                            + "&jumlah=" + jumlah
                            + "&pengguna=" + pengguna;
        transaksiController.perform(MockMvcRequestBuilders.post( urlTemplate))
                .andExpect(status().isOk());

        tipe = "Pemasukan";
        judul = "Narik Gojek";
        kategori = "Kerja";
        jumlah = 20000;
        urlTemplate = "/addtransaksi?tipe="+ tipe
                + "&judul=" + judul
                + "&kategori=" + kategori
                + "&jumlah=" + jumlah
                + "&pengguna=" + pengguna;
        transaksiController.perform(MockMvcRequestBuilders.post( urlTemplate))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAllPengeluaran() throws Exception {
        transaksiController.perform(MockMvcRequestBuilders.get("/get-all-pengeluaran"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAllPemasukan() throws Exception {
        transaksiController.perform(MockMvcRequestBuilders.get("/get-all-pemasukan"))
                .andExpect(status().isOk());
    }

    @Test
    public void testModelPengeluaranAndDelete() throws Exception {
        String sDate1="31/12/1998";
        Date date =new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        Pengeluaran dummy = new Pengeluaran(
                date,
                "Judul",
                "Kategori",
                10000,
                "Pengguna"
        );
        dummy.setId(2);
        dummy.setTanggalCatatan(date);
        dummy.setJudul("Makan Siang");
        dummy.setKategori("Makanan");
        dummy.setJumlah(15000);
        dummy.setPengguna("100117");
        Assert.assertEquals(2,dummy.getId());
        Assert.assertEquals(date,dummy.getTanggalCatatan());
        Assert.assertEquals("Makan Siang", dummy.getJudul());
        Assert.assertEquals("Makanan", dummy.getKategori());
        Assert.assertEquals(15000,dummy.getJumlah());
        Assert.assertEquals("100117",dummy.getPengguna());

        transaksiController.perform(
                MockMvcRequestBuilders.post("/delete-pengeluaran/2?pengguna=100117"))
                .andExpect(status().isOk());
    }

    @Test
    public void testModelPemasukan() throws Exception {
        String sDate1="31/12/1998";
        Date date =new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        Pemasukan dummy = new Pemasukan(
                date,
                "Judul",
                "Kategori",
                10000,
                "Pengguna"
        );
        dummy.setId(2);
        dummy.setTanggalCatatan(date);
        dummy.setJudul("Narik Gojek");
        dummy.setKategori("Kerja");
        dummy.setJumlah(15000);
        dummy.setPengguna("100117");
        Assert.assertEquals(2,dummy.getId());
        Assert.assertEquals(date,dummy.getTanggalCatatan());
        Assert.assertEquals("Narik Gojek", dummy.getJudul());
        Assert.assertEquals("Kerja", dummy.getKategori());
        Assert.assertEquals(15000,dummy.getJumlah());
        Assert.assertEquals("100117",dummy.getPengguna());

        transaksiController.perform(
                MockMvcRequestBuilders.post("/delete-pemasukan/2?pengguna=100117"))
                .andExpect(status().isOk());
    }
}
